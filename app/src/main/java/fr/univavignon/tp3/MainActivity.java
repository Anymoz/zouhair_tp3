package fr.univavignon.tp3;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    SportDbHelper sportDbHelper = new SportDbHelper(this);

    SwipeRefreshLayout swipeRefreshLayout;

    Cursor allTeams;
    VerticalRecyclerViewAdapter adapter2;
    RecyclerView recyclerView;
    public static List<Team> teamsArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

            FloatingActionButton fab = findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent ajout_Intent = new Intent(MainActivity.this, NewTeamActivity.class);
                    startActivity(ajout_Intent);
                }
            });

        sportDbHelper.populate();

        allTeams = sportDbHelper.fetchAllTeams();

        teamsArray = sportDbHelper.getAllTeams();
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new AsyncAllRefresh().execute();
            }
        });

        // ItemTouchHelper
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                int dragFlags = 0;
                int swipeFlags = ItemTouchHelper.LEFT;
                return makeMovementFlags(dragFlags, swipeFlags);
            }

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {

                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                long id = teamsArray.get(position).getId();
                Team delTeam = sportDbHelper.getTeam(id);
                sportDbHelper.deleteTeam(id);
                teamsArray.remove(position);
                teamsArray = sportDbHelper.getAllTeams();
                adapter2.notifyItemRemoved(position);
            }

            @Override
            public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
                if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                    viewHolder.itemView.setBackgroundColor(Color.LTGRAY);
                }
                super.onSelectedChanged(viewHolder, actionState);
            }

            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                super.clearView(recyclerView, viewHolder);
                viewHolder.itemView.setBackgroundColor(0);
            }
        });

        // RecyclerView
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager= new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter2 = new VerticalRecyclerViewAdapter();
        recyclerView.setAdapter(adapter2);
        itemTouchHelper.attachToRecyclerView(recyclerView);


        // Request permission
        if (Build.VERSION.SDK_INT >= 23) {
            int REQUEST_CODE_CONTACT = 101;
            String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
            for (String str : permissions) {
                if (this.checkSelfPermission(str) != PackageManager.PERMISSION_GRANTED) {
                    this.requestPermissions(permissions, REQUEST_CODE_CONTACT);
                    return;
                }
            }
        }
    }

    @Override
    public void onResume() {

        super.onResume();
        teamsArray = sportDbHelper.getAllTeams();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private final class AsyncAllRefresh extends AsyncTask<String, Void, List<Team>> {


        @Override
        protected List<Team> doInBackground(String... strings) {
            JSONResponseHandlerTeam jsonResponse = null;
            MiseAjourInf miseAjourInf = null;
            for (Team t : teamsArray) {
                jsonResponse = new JSONResponseHandlerTeam(t);
                miseAjourInf = new MiseAjourInf(jsonResponse, t);
                miseAjourInf.execute();
            }
            return teamsArray;
        }

        @Override
        protected void onPostExecute(List<Team> result) {
            swipeRefreshLayout.setRefreshing(false);

            teamsArray = result;
            for (int i=0;i<teamsArray.size();i++) {
                sportDbHelper.updateTeam(teamsArray.get(i));
                adapter2.notifyItemChanged(i);
            }
            Toast.makeText(MainActivity.this, "Bien mise à jour de toutes les équipes", Toast.LENGTH_LONG).show();
        }
    }

    class VerticalRecyclerViewAdapter extends RecyclerView.Adapter<VerticalRecyclerViewAdapter.MyViewHolder>{

        public VerticalRecyclerViewAdapter() {
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.content_listview,parent,false);
            final MyViewHolder holder = new MyViewHolder(view);
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = holder.getAdapterPosition();
                    Team teamSend = teamsArray.get(position);
                    Intent intent = new Intent(MainActivity.this, TeamActivity.class);
                    intent.putExtra("teamSend", teamSend);
                    startActivity(intent);
                }
            });

            return holder;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            Team tmp = teamsArray.get(position);
            Long teamId = tmp.getId();
            holder.teamName.setText(tmp.getName());
            holder.teamLigue.setText(tmp.getLeague());
            holder.teamLastMatch.setText(tmp.getLastEvent().toString());
            if (teamId!=0) {
                Log.e("->>>>>>>>>> ","Leaague : "+tmp.getLeague()+"Name   : "+tmp.getName()+"Teambadge : "+tmp.getTeamBadge());
                loadMapPreview(holder.teamImage,tmp.getTeamBadge());
            }
        }
        public void loadMapPreview (final ImageView imageView, final String url) {
            //start a background thread for networking
            new Thread(new Runnable() {
                public void run(){
                    try {
                        //download the drawable
                        final Drawable drawable = Drawable.createFromStream((InputStream) new URL(url).getContent(), "src");
                        //edit the view in the UI thread
                        imageView.post(new Runnable() {
                            public void run() {
                                imageView.setImageDrawable(drawable);
                            }
                        });
                    }
                    catch
                        (IOException e) {
                        e.printStackTrace();
                             }
                }
            }).start();
        }

        @Override
        public int getItemCount() {
            return teamsArray == null ? 0 : teamsArray.size();
        }


        class MyViewHolder extends RecyclerView.ViewHolder
            {
                TextView teamName,teamLigue,teamLastMatch;
                ImageView teamImage;
                View view;

            public MyViewHolder(View itemView)
            {
                super(itemView);
                this.view = itemView;
                teamImage = view.findViewById(R.id.teamImage);
                teamName = view.findViewById(R.id.teamName);
                teamLigue = view.findViewById(R.id.teamLigue);
                teamLastMatch = view.findViewById(R.id.teamLastMatch);
            }
        }
    }
}
