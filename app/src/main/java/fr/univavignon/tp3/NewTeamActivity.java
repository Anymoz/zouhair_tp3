package fr.univavignon.tp3;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class NewTeamActivity extends AppCompatActivity {

    private EditText TeamName, textLeague;
    SportDbHelper sportDbHelper = new SportDbHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_team);

        TeamName = (EditText) findViewById(R.id.editNewName);
        textLeague = (EditText) findViewById(R.id.editNewLeague);

        final Button but = (Button) findViewById(R.id.update_btn);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                Team team = new Team(TeamName.getText().toString(), textLeague.getText().toString());

                if (NewTeamActivity.this.TeamName.getText().toString().isEmpty()) {
                    new AlertDialog.Builder(NewTeamActivity.this)
                            .setTitle("Sauvegarde impossible")
                            .setMessage("Le nom de l'équipe doit être non vide.")
                            .show();
                } else {
                    if(sportDbHelper.addTeam(team) == false){
                        new AlertDialog.Builder(NewTeamActivity.this)
                                .setTitle("Sauvegarde impossible")
                                .setMessage("Le nom de l'équipe et le nom de league existent.")
                                .show();
                    }
                    else {
                        intent.putExtra(Team.TAG, team);
                        setResult(RESULT_OK, intent);
                        Toast.makeText(NewTeamActivity.this, "Ajouté avec succsés", Toast.LENGTH_LONG).show();
                        finish();
                    }
                }
            }
        });
    }
}
