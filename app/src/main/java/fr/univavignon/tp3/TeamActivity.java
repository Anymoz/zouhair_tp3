package fr.univavignon.tp3;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;


    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    public static ImageView imageBadge;
    private Team team;

    JSONResponseHandlerTeam jsonResponse = null;
    SportDbHelper sportDbHelper = new SportDbHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra("teamSend");
        jsonResponse = new JSONResponseHandlerTeam(this.team);

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);
        imageBadge = (ImageView) findViewById(R.id.TeamBadge);

        updateView();

        final Button update_btn = (Button) findViewById(R.id.update_btn);

        update_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO
                new AsyncTaskRunner().execute(team.getTeamBadge());
            }
        });

    }

    private final class AsyncTaskRunner extends AsyncTask<String, Void, Void>{


        @Override
        protected Void doInBackground(String... urls) {
            MiseAjourInf miseAjourInf = new MiseAjourInf(jsonResponse,team);
            miseAjourInf.execute();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            sportDbHelper.updateTeam(team);
            updateView();
        }
    }



    @Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity
        Intent intent = new Intent(TeamActivity.this, MainActivity.class);
        startActivity(intent);
        super.onBackPressed();
    }

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

        if (team.getIdTeam() != 0){
            loadMapPreview(imageBadge,team.getTeamBadge());
        }
    }

    public void loadMapPreview (final ImageView imageView, final String url) {
        //start a background thread for networking
        new Thread(new Runnable() {
            public void run(){
                try {
                    //download the drawable
                    final Drawable drawable = Drawable.createFromStream((InputStream) new URL(url).getContent(), "src");
                    //edit the view in the UI thread
                    imageView.post(new Runnable() {
                        public void run() {
                            imageView.setImageDrawable(drawable);
                        }
                    });
                }
                catch
                (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
