package fr.univavignon.tp3;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Date;


/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=R
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerTeam {

    private static final String TAG = JSONResponseHandlerTeam.class.getSimpleName();

    private Team team;
    private Match match = new Match();


    public JSONResponseHandlerTeam(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response)  {

        try {
            JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
            readTeams(reader);
        } catch (Exception e) {
        }
    }

    public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("teams")  && reader.peek() != JsonToken.NULL) {
                readArrayTeams(reader);
            }else if (name.equals("table")){
                readArrayTable(reader);
            } else if (name.equals("results")){
                readArrayResults(reader);
            }
            else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }
    private void readArrayTable(JsonReader reader) throws IOException {
        reader.beginArray();
        int i = 0;
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();

                if (name.equals("teamid")){
                    Long teamid = reader.nextLong();
                    if (teamid.equals(team.getIdTeam())) {

                        team.setRanking(i+1);
                        while (reader.hasNext()){
                            name = reader.nextName();
                            if (name.equals("total")){
                                team.setTotalPoints(reader.nextInt());
                            } else{
                                reader.skipValue();
                            }
                        }
                    }
                } else{
                    reader.skipValue();
                }
            }
            i++;
            reader.endObject();
        }
        reader.endArray();
    }

    private void readArrayTeams(JsonReader reader) throws IOException {
        if(reader.peek() != JsonToken.NULL) {
            reader.beginArray();

            int nb = 0; // only consider the first element of the array
            while (reader.hasNext()) {
                reader.beginObject();
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    if (nb == 0) {
                        if (name.equals("idTeam")) {
                            team.setIdTeam(reader.nextLong());
                        } else if (name.equals("strTeam")) {
                            team.setName(reader.nextString());
                        } else if (name.equals("strLeague")) {
                            team.setLeague(reader.nextString());
                        } else if (name.equals("idLeague")) {
                            team.setIdLeague(reader.nextLong());
                        } else if (name.equals("strStadium")) {
                            team.setStadium(reader.nextString());
                        } else if (name.equals("strStadiumLocation")) {
                            team.setStadiumLocation(reader.nextString());
                        } else if (name.equals("strTeamBadge")) {
                            team.setTeamBadge(reader.nextString());
                        } else {
                            reader.skipValue();
                        }
                    } else {
                        reader.skipValue();
                    }
                }
                reader.endObject();
                nb++;
            }
            reader.endArray();
        }
    }

    private void readArrayResults(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = "";
                if (reader.peek() != JsonToken.NULL) {
                    name = reader.nextName();
                }
                if (nb == 0) {
                    if (name.equals("idEvent")) {
                        match.setId(reader.nextLong());
                    } else if (name.equals("strEvent")) {
                        match.setLabel(reader.nextString());
                    } else if (name.equals("strHomeTeam")){
                        match.setHomeTeam(reader.nextString());
                    } else if (name.equals("strAwayTeam")){
                        match.setAwayTeam(reader.nextString());
                    } else if (name.equals("intHomeScore")){
                        if (reader.peek() != JsonToken.NULL){
                            match.setHomeScore(reader.nextInt());
                        }else{
                            match.setHomeScore(0);
                        }
                    } else if (name.equals("intAwayScore")){
                        if (reader.peek() != JsonToken.NULL){
                            match.setAwayScore(reader.nextInt());
                        }else{
                            match.setAwayScore(0);
                        }
                    } else {
                        reader.skipValue();
                    }
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
        team.setLastEvent(match);
    }
}
